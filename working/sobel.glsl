uniform sampler2D InTexture;
uniform mat3 LeftKernel;
uniform mat3 RightKernel;
uniform float CutOff;
uniform mat4 Projection;
uniform mat4 Transform;

smooth VSData vec2 TexPos;

const float PI = 3.14159;

float intensity(in vec4 p)
{
	return (p.r + p.g + p.b) / 3.0f;
}

float convolve(in mat3 m, in sampler2D tex, in vec2 p)
{
	float result = 0;
	for(int x = -1; x < 2; x++)
	{
		for(int y = -1; y < 2; y++)
		{
			result += m[x + 1][y + 1] * intensity(textureOffset(tex, p, ivec2(x, y)));
		}
	}
	return result;
}

#ifdef VERTEX_SHADER
in vec4 Vertex;

void main()
{
	gl_Position = Projection*Transform*Vertex;
	TexPos = Vertex.xy; // [-1, 1] -> [0, 1]

}


#elif defined FRAGMENT_SHADER
out vec4 FsColor;

void main()
{
	float xinten = convolve(LeftKernel, InTexture, TexPos);
	float yinten = convolve(RightKernel, InTexture, TexPos);
	float result = sqrt(xinten*xinten + yinten*yinten);
	float angle = atan(yinten, xinten + 0.00001);
	FsColor = vec4(xinten, yinten, angle, result);
}

#endif