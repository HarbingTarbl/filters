#include "CompiledHeader.hpp"

#include "Image.hpp"
#include "Texture.hpp"



PGMImage::PGMImage(const fi::path& path)
	: buffer(0), ActualMax(0), PixelSize(0), MaxValue(0)
{
	fstream in(path, fstream::binary | fstream::in);

	char magic[2];
	in.read(magic, 2);
	if(magic[0] != 'P' || magic[1] != '5')
		throw runtime_error(path.string() + " is not a valid PGM file");

	in >> width >> height >> MaxValue;
	in.get(); //Skip single whitespace

	if(MaxValue > 255)
		PixelSize = 2;
	else
		PixelSize = 1;

	buffer.reserve(width * height);

	for(int i = 0; i < width * height; i++)
	{
		unsigned short value = 0;
		in.read(reinterpret_cast<char*>(&value), PixelSize);
		if(value > ActualMax)
			ActualMax = value;
		buffer.push_back(static_cast<float>(static_cast<double>(value) / static_cast<double>(MaxValue)));
	}
}

SOILImage::SOILImage(const fi::path& path)
{
	unsigned char* x = SOIL_load_image(path.string().c_str(), &width, &height, &channels, 0);
	buffer.reset(x);
	switch(channels)
	{
	case SOIL_LOAD_RGB:
		bufferSize = width * height * 3 * sizeof(unsigned char);
		break;
	case SOIL_LOAD_RGBA:
		bufferSize = width * height * 4 * sizeof(unsigned char);
		break;
	default:
		throw runtime_error("Unknown file type for image " + path.string());
	}
}


unique_ptr<Image> Image::FromFile(const fi::path& path)
{
	if(path.extension() == ".pgm")
		return PGMImage::FromFile(path);
	else
		return SOILImage::FromFile(path);
}

unique_ptr<PGMImage> PGMImage::FromFile(const fi::path& path)
{
	return unique_ptr<PGMImage>(new PGMImage(path));
}

unique_ptr<PGMImage> PGMImage::FromFramebuffer() 
{
	return unique_ptr<PGMImage>();
}

unique_ptr<SOILImage> SOILImage::FromFile(const fi::path& path)
{
	return unique_ptr<SOILImage>(new SOILImage(path));
}

unique_ptr<SOILImage> SOILImage::FromFramebuffer()
{
	return unique_ptr<SOILImage>();
}

unique_ptr<Texture> PGMImage::CreateTexture()
{
	return unique_ptr<Texture>(new PGMTexture(this));
}

unique_ptr<Texture> SOILImage::CreateTexture()
{
	return unique_ptr<Texture>(new SOILTexture(this));
}