uniform sampler2D InTexture;
uniform mat4 Projection;
uniform mat4 Transform;

smooth VSData vec2 TexPos;

#ifdef VERTEX_SHADER
in vec4 Vertex;


void main()
{
	gl_Position = Projection*Transform*Vertex;
	TexPos = Vertex.xy; // [-1, 1] -> [0, 1]

}


#elif defined FRAGMENT_SHADER
out vec4 FsColor;

void main()
{
	FsColor =  vec4(texture(InTexture, TexPos));
}

#endif