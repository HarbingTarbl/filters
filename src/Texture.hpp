#pragma once
#include "CompiledHeader.hpp"

class PGMImage;
class SOILImage;
class Image;

class Texture
{
	friend class Image;
protected:
	Texture();
	Texture(const Image* image);
	unsigned width;
	unsigned height;

public:
	virtual unsigned GetId() const = 0;
	virtual void Bind(unsigned site) const = 0;

	virtual unsigned Width() const { return width; }
	virtual unsigned Height() const { return height; }

	~Texture() {}
};


class GaussTexture
	: public Texture
{
private:
	unsigned textureId;
	unsigned sigma;
	unsigned size;

public:

	GaussTexture(unsigned sigma);
	~GaussTexture();

	unsigned GetId() const { return textureId; }
	void Bind(unsigned site) const;

	unsigned GetWidth() const { return size; }
	unsigned GetHeight() const { return size; }
	unsigned GetSigma() const { return sigma; }
};

class SOILTexture
	: public Texture
{
	friend class SOILImage;
private:
	SOILTexture(const SOILImage* image);
	unsigned textureId;

public:
	~SOILTexture();
	unsigned GetId() const { return textureId; }
	void Bind(unsigned site) const;
};

class PGMTexture
	: public Texture
{
	friend class PGMImage;

private:
	unsigned textureId;
	PGMTexture(const PGMImage* image);


public:
	unsigned short MaxValue;

	~PGMTexture();
	unsigned GetId() const { return textureId; }
	void Bind(unsigned unit) const;
};
