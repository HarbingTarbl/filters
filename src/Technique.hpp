#pragma once
#ifndef _TECHNIQUE_HPP
#define _TECHNIQUE_HPP
#include "CompiledHeader.hpp"
#include "Framebuffer.hpp"
#include "Texture.hpp"
#include "Effect.hpp"

#include <cmath>

class Technique
{
public:
	virtual void PreApply() {}
	virtual void Apply() = 0;
	virtual void PostApply() {}
};

class ReductionTechnique
{
private:
	FramebufferTexture frontBuffer;
	FramebufferTexture backBuffer;
	unsigned currentBuffer;

	unique_ptr<Effect> reductionEffect;
	unsigned kernelSize;

	unsigned bufferId;
	unsigned vaoId;

	Texture* sourceTexture;

	void drawQuad(unsigned x, unsigned y, unsigned width, unsigned height)
	{
		const float b = y;
		const float t = y + height;
		const float l = x;
		const float r = x + width;

		float quadVert[] = {
			l, b, 
			r, b,
			l, t,
			r, t,
		};

		static_assert(sizeof(fvec2) * 4 == sizeof(quadVert), "Bad Size");

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVert), NULL, GL_STREAM_DRAW);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVert), quadVert, GL_STREAM_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(vaoId);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	void genQuad()
	{
		glGenVertexArrays(1, &vaoId);
		glGenBuffers(1, &bufferId);

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(fvec2) * 4, NULL, GL_STREAM_DRAW);
		glBindVertexArray(vaoId);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);
		glBindVertexArray(0);
		glDisableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	}

	void initReduction()
	{
		reductionEffect->Bind();
		reductionEffect->Uniform("Mode", 0);
		sourceTexture->Bind(GL_TEXTURE0);
		GetWriteBuffer().BindWrite();
		drawQuad(0, 0, sourceTexture->Width(), sourceTexture->Height());
		SwapBuffers();
	}

public:
	ReductionTechnique(unique_ptr<Effect> effect, unsigned kernSize, unsigned width, unsigned height, GLenum internalFormat, GLenum format)
		: 
		frontBuffer(width, height, internalFormat, format), 
		backBuffer(width, height, internalFormat, format),
		currentBuffer(0),
		reductionEffect(move(effect)),
		kernelSize(kernSize)
	{
		genQuad();
		reductionEffect->Bind();
		reductionEffect->Uniform("ReductionTexture", 0);
		reductionEffect->Uniform("Orthographic", glm::ortho<float>(0, width , 0, height ));

		glBindTexture(GL_TEXTURE_2D, frontBuffer.GetTexture());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glBindTexture(GL_TEXTURE_2D, backBuffer.GetTexture());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	inline void SwapBuffers()
	{
		currentBuffer = !currentBuffer;
	}

	inline FramebufferTexture& GetReadBuffer()
	{
		return currentBuffer == 0 ? backBuffer : frontBuffer;
	}

	inline FramebufferTexture& GetWriteBuffer()
	{
		return currentBuffer == 0 ? frontBuffer : backBuffer;
	}

	inline Effect& Effect()
	{
		return *reductionEffect;
	}

	inline void SetSourceTexture(Texture* tex)
	{
		sourceTexture = tex;
	}

	void Apply()
	{
		using namespace std;
		glPushAttrib(GL_ENABLE_BIT);
		glDisable(GL_STENCIL_TEST);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		initReduction();
		reductionEffect->Bind();
		reductionEffect->Uniform("KernelSize", kernelSize);
		reductionEffect->Uniform("Mode", 1);
		unsigned cWidth, cHeight;

		cWidth = sourceTexture->Width() / kernelSize;
		cHeight = sourceTexture->Height() / kernelSize;

		int iter = 2;
		while(cWidth > 1 || cHeight > 1)
		{
			GetReadBuffer().BindRead(GL_TEXTURE0);
			GetWriteBuffer().BindWrite();

			reductionEffect->Uniform("Iteration", iter);
			reductionEffect->Uniform("CWidth", cWidth);
			reductionEffect->Uniform("CHeight", cHeight);
			reductionEffect->Uniform("TexelSize", fvec2( 1.0f / cWidth, 1.0f / cHeight));
			drawQuad(0, 0, cWidth, cHeight);

			cWidth = max<unsigned>(cWidth / kernelSize, 1);
			cHeight = max<unsigned>(cHeight / kernelSize, 1);

			iter *= iter;
			SwapBuffers();
		}

		glPopAttrib();
	}

	void GetResult(void* data, unsigned format, unsigned pixel)
	{
		glBindFramebuffer(GL_READ_FRAMEBUFFER, GetReadBuffer().GetFBuffer());
		glReadPixels(0, 0, 1, 1, format, pixel, data);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	}
};




#endif