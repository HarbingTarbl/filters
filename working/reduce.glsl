uniform sampler2D ReductionTexture;
uniform vec2 TexelSize;
uniform int Iteration;
uniform int Mode; // 0 == initalize, 1 == reduce

smooth VSData vec2 TexPos;

uniform mat4 Orthographic;

#ifdef VERTEX_SHADER
in vec2 Vertex;

void main()
{
	vec4 result = Orthographic * vec4(Vertex, 0, 1);
	gl_Position = result;
	TexPos = Vertex;
}


#elif defined FRAGMENT_SHADER
out vec2 FsColor;

void main()
{
	ivec2 frag = ivec2(TexPos);
	if(Mode == 0)
	{
		FsColor = texelFetch(ReductionTexture, frag, 0).rr;
		return;
	}
	
	frag *= 2;	
	vec2 t1 = texelFetch(ReductionTexture, frag, 0).rg;
	vec2 t2 = texelFetch(ReductionTexture, frag + ivec2(0, 1), 0).rg;
	vec2 t3 = texelFetch(ReductionTexture, frag + ivec2(1, 1), 0).rg;
	vec2 t4 = texelFetch(ReductionTexture, frag + ivec2(1, 0), 0).rg;
	
	FsColor.r = max(t1.r,max(t2.r,max(t3.r,t4.r)));
	FsColor.g = min(t1.g,min(t2.g,min(t3.g,t4.g)));
}

#endif