//Non Maximal Suppression

uniform sampler2D InTexture;
uniform vec2 InTexTexels;

uniform vec4 PeakColor;

uniform float HighThresh;
uniform float LowThresh;

uniform mat4 Projection;
uniform mat4 Transform;

smooth VSData vec2 TexPos;

#ifdef VERTEX_SHADER
in vec4 Vertex;

void main()
{
	gl_Position = Projection*Transform*Vertex;
	TexPos = Vertex.xy; // [-1, 1] -> [0, 1]
	TexPos.y = 1 - Vertex.y;
}


#elif defined FRAGMENT_SHADER
out vec4 FsColor;

void main()
{
	vec4 sample = texture(InTexture, TexPos);
	//blue channel == angle of edge

	vec2 edgeDir = (sample.rg/sample.aa) * InTexTexels;
	//vec2 edgeDir = vec2((sample.r / sample.g));
	//vec2 edgeDir = vec2(cos(sample.b), sin(sample.b)) * InTexTexels;
	vec2 nbors = vec2(
		texture(InTexture, TexPos + edgeDir).a, 
		texture(InTexture, TexPos - edgeDir).a
		);
		
	
	
		
	FsColor = vec4(0);

	if(sample.a < nbors.x)
		return;
		
	if( sample.a < nbors.y)
		return;
		
	if(nbors.x < LowThresh)
		return;
		
	if(nbors.y < LowThresh)
		return;
		
	FsColor = PeakColor;
}

#endif