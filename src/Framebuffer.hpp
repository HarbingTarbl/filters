#pragma once
#ifndef _FRAMEBUFFER_HPP
#define _FRAMEBUFFER_HPP
#include "CompiledHeader.hpp"
#include "Texture.hpp"

class Framebuffer //Todo fill this out, maybe have an actual framebuffer inheritance system? Maybe not, always gets
	//Annoying when you over abstract. 
{
protected:
	unsigned bufferId;

public:
	virtual unsigned GetId() const { return bufferId; }
	virtual void BindRead(unsigned unit);
	virtual void BindWrite();

	virtual ~Framebuffer();
};

class FramebufferTexture
{
private:
	unsigned framebufferId;
	unsigned textureId;

public:
	const unsigned Width;
	const unsigned Height;

	FramebufferTexture(unsigned width, unsigned height, GLenum internalFormat, GLenum format)
		: Width(width), Height(height)
	{
		glGenFramebuffers(1, &framebufferId);
		glGenTextures(1, &textureId);

		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, Width, Height, 0, format, GL_BYTE, nullptr);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
		GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, drawBuffers);
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			throw runtime_error("Incomplete framebuffer");
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	~FramebufferTexture()
	{
		glDeleteFramebuffers(1, &framebufferId);
		glDeleteTextures(1, &textureId);
	}

	unsigned GetTexture() const 
	{
		return textureId;
	}

	unsigned GetFBuffer() const
	{
		return framebufferId;
	}

	void BindRead(unsigned unit) const
	{
		glActiveTexture(unit);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}

	void BindWrite() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
		glViewport(0, 0, Width, Height);
	}

};




#endif