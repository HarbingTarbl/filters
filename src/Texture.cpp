#include "CompiledHeader.hpp"
#include "Texture.hpp"
#include "Image.hpp"


Texture::Texture()
	: width(0), height(0)
{

}

Texture::Texture(const Image* image)
	: width(image->GetWidth()), height(image->GetHeight())
{

}

PGMTexture::PGMTexture(const PGMImage* image)
	: Texture(image)
{
	if(image == nullptr)
		return;

	MaxValue = image->MaxValue;
	width = image->width;
	height = image->height;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	int mask[] = { GL_RED, GL_RED, GL_RED, GL_RED };
	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, mask);


	glTexImage2D(GL_TEXTURE_2D, 0, GL_R16, image->width, image->height, 0, GL_RED, GL_FLOAT, image->Buffer());
	glBindTexture(GL_TEXTURE_2D, 0);
}

PGMTexture::~PGMTexture()
{
	glDeleteTextures(1, &textureId);
}


void PGMTexture::Bind(unsigned unit) const
{
	glActiveTexture(unit);
	glBindTexture(GL_TEXTURE_2D, textureId);
}

void SOILTexture::Bind(unsigned unit) const
{
	glActiveTexture(unit);
	glBindTexture(GL_TEXTURE_2D, textureId);
}

SOILTexture::SOILTexture(const SOILImage* image)
	: Texture(image)
{
	textureId = SOIL_create_OGL_texture(image->buffer.get(), image->GetWidth(), image->GetHeight(), image->GetChannels(), 0, SOIL_FLAG_INVERT_Y);

	if(textureId == 0)
		throw runtime_error("Could not create SOILTexture from SOILImage");
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

}

SOILTexture::~SOILTexture()
{
	glDeleteTextures(1, &textureId);
}

GaussTexture::GaussTexture(unsigned sigma)
	: sigma(sigma), size(2 * sigma + 1), Texture()
{
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_1D, textureId);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	vector<float> buffer;
	buffer.resize(size * 2);
	//Buffer stuff

	glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, size, 0, GL_R, GL_FLOAT, buffer.data());



}

void GaussTexture::Bind(unsigned unit) const
{
	glActiveTexture(unit);
	glBindTexture(GL_TEXTURE_1D, textureId);
}

GaussTexture::~GaussTexture()
{
	glDeleteTextures(1, &textureId);
}