#pragma once
#include "CompiledHeader.hpp"

class Window
{
private:


	GLFWwindow* handle;

	int width, height;
	int fbwidth, fbheight;
	int xpos, ypos;
	double moux, mouy;
	double lastTime;

	static void ProxyResize(GLFWwindow* window, int w, int h);
	static void ProxyMove(GLFWwindow* window, int x, int y);
	static void ProxyRefresh(GLFWwindow* window);
	static void ProxyFocus(GLFWwindow* window, int f);
	static void ProxyIconified(GLFWwindow* window, int i);
	static void ProxyClose(GLFWwindow* window);
	static void ProxyFBResize(GLFWwindow* window, int w, int h);
	static void ProxyMouseButton(GLFWwindow* window, int b, int a, int m);
	static void ProxyScroll(GLFWwindow* window, double x, double y);
	static void ProxyChar(GLFWwindow* window, unsigned p);
	static void ProxyEnter(GLFWwindow* window, int e);
	static void ProxyMouseMove(GLFWwindow* window, double x, double y);
	static void ProxyKey(GLFWwindow* window, int k, int s, int a, int m);

protected:
	virtual void OnResize(int nwidth, int nheight);
	virtual void OnMove(int newx, int newy);
	virtual void OnRefresh();
	virtual void OnFocus(int focused);
	virtual void OnIconified(int iconed);
	virtual void OnClose();
	virtual void OnFBResize(int nwidth, int nheight);
	virtual void OnMouseButton(int button, int action, int mods);
	virtual void OnScroll(double xscroll, double yscroll);
	virtual void OnChar(unsigned int point);
	virtual void OnEnter(int entered);
	virtual void OnMouseMove(double newx, double newy);
	virtual void OnKey(int key, int scan, int action, int mods);

public:
	static bool openglInitalized;
	static bool glfwInitalized;
	Window(const string& name, int width, int height, GLFWwindow* share = nullptr, GLFWmonitor* mon = nullptr);
	~Window();

	GLFWwindow* GetHandle() const;

	int GetWidth() const;
	int GetHeight() const;
	
	void SetWidth(int width);
	void SetHeight(int height);

	int GetX() const;
	int GetY() const;
	
	void SetX(int x);
	void SetY(int y);

	double GetMouseX() const;
	double GetMouseY() const;

	void SetMouseX(double x);
	void SetMouseY(double y);

	void Show(bool visible);
	bool IsOpen() const;
	void Close();

	void SwapBuffer() const;

	virtual void OnUpdate(double ttime, double elapsed) = 0;
	virtual void OnRender(double ttime, double elapsed) = 0;
};