dofile("glsdk/links.lua")

solution "Canny"
	location "build"
	debugdir "working"
	configurations {"Debug", "Release"}
	platforms {"windows"}
	
	project "Canny"
		kind "ConsoleApp"
		language "c++"
		libdirs {"lib"}
		includedirs {"include"}
		files {"src/**.cpp", "src/**.hpp"}
		
		UseLibs {"glm",  "glutil"}
		
		
		objdir "bin/obj"
		targetdir "bin"

		configuration "windows"
			defines "WIN32"
			links 
			{
				"glu32", 
				"opengl32", 
				"gdi32", 
				"winmm", 
				"user32", 
				"AntTweakBar", 
				"glfw3", 
				"glew32s", 
				"assimp",
				"glfx",
				"SOIL",
			}
			
		configuration "Debug"
			targetsuffix "D"
			defines "_DEBUG"
			optimize "Off"
			flags "Symbols"

		configuration "Release"
			defines "NDEBUG"
			optimize "Speed"
			warnings "Extra"
			flags { "NoFramePointer", "NoEditAndContinue"}
