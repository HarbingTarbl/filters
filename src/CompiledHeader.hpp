#pragma once 
#define GLEW_STATIC
#define GLFX_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/glfx.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <iostream>
#include <fstream>
#include <memory>
#include <algorithm>
#include <vector>
#include <exception>
#include <unordered_map>


namespace po = boost::program_options;
namespace fi = boost::filesystem;

using std::string;
using std::vector;
using std::unique_ptr;
using std::runtime_error;
using std::weak_ptr;
using std::unordered_map;
using std::move;
using std::cout;
using std::endl;

using fi::fstream;

using glm::fvec2;
using glm::fvec3;
using glm::fvec4;
using glm::fmat3;
using glm::fmat4;