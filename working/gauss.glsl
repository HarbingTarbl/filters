uniform sampler2D InTexture;
uniform sampler1D DGaussTexture;

uniform int Sigma;
uniform float CutOff;
uniform float LowerCutOff;

uniform vec2 InTexTexels;
uniform vec2 DGaussTexTexels;

uniform mat4 Projection;
uniform mat4 Transform;

const float PI2 = 2 * 3.14159;

smooth VSData vec2 TexPos;

float intensity(in vec4 p)
{
	return (p.r + p.g + p.b) / 3.0f;
}

vec2 dgauss(in vec2 pos)
{
	float lsqr = dot(pos, pos);
	float sigsqr = Sigma*Sigma;
	return (pos*exp(-lsqr/(2.0*sigsqr)));
}

#ifdef VERTEX_SHADER
in vec4 Vertex;

void main()
{
	gl_Position = Projection*Transform*Vertex;
	TexPos = Vertex.xy; //
}


#elif defined FRAGMENT_SHADER
out vec4 FsColor;

void main()
{
	vec2 deriv = vec2(0);
	for(int x = -Sigma; x <= Sigma; x++)
	{
		for(int y = -Sigma; y <= Sigma; y++)
		{
			vec2 offset = vec2(x, y);
			deriv += dgauss(offset)*intensity(texture(InTexture, (TexPos + offset*InTexTexels), 0));
		}
	}
	
	
	float inten = length(deriv);
	float angle = atan(deriv.y, deriv.x);
	FsColor= vec4(deriv, angle, inten);
}

#endif