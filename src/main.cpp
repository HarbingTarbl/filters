#include "CompiledHeader.hpp"
#include "Texture.hpp"
#include "Image.hpp"
#include "Effect.hpp"

string FileIn;
string FileOut;

void InitOpenGL()
{
	int error;
	glewExperimental = GL_TRUE;
	error = glewInit();
	if(error != GLEW_OK)
	{
		throw runtime_error("Unable to initalize opengl " + string(reinterpret_cast<const char*>(glewGetErrorString(error))));
	}

	std::cout << "Loaded OpenGL " << glGetString(GL_VERSION) << std::endl;
}

void HandleArguments(int argc, char* args[])
{
	po::options_description op("Options");
	op.add_options()
		("input", po::value<string>(), "input image file")
		("output", po::value<string>(), "output image file");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, args, op), vm);

	if(!vm.count("input"))
		throw runtime_error("Input file must be specified");

	fi::path inputPath(vm["input"].as<string>());

	FileIn = inputPath.string();

	if(!vm.count("output"))
	{
		FileOut = "output.pgm";
	}
	else
	{
		FileOut = fi::path(vm["output"].as<string>()).string();
	}
}


class FramebufferTexture
{
private:
	unsigned framebufferId;
	unsigned textureId;

public:
	const unsigned Width;
	const unsigned Height;

	FramebufferTexture(unsigned width, unsigned height, GLenum internalFormat, GLenum format)
		: Width(width), Height(height)
	{
		glGenFramebuffers(1, &framebufferId);
		glGenTextures(1, &textureId);

		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, Width, Height, 0, format, GL_BYTE, nullptr);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
		GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, drawBuffers);
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			throw runtime_error("Incomplete framebuffer");
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	~FramebufferTexture()
	{
		glDeleteFramebuffers(1, &framebufferId);
		glDeleteTextures(1, &textureId);
	}

	void BindRead(unsigned unit) const
	{
		glActiveTexture(unit);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}

	void BindWrite() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
		glViewport(0, 0, Width, Height);
	}

};

class FFQuad
{
private:
	unsigned vao;
	unsigned vbuffer;

public:

	FFQuad()
	{
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, VertexData, GL_STATIC_DRAW);


		glBindVertexArray(vao);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, 0);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	~FFQuad()
	{
		glDeleteBuffers(1, &vbuffer);
		glDeleteVertexArrays(1, &vao);
	}

	void Draw() const
	{
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	static float VertexData[];
};

float FFQuad::VertexData[] = 
{
	0, 0, 0, 1, 
	1, 0, 0, 1,
	1, 1, 0, 1,
	1, 1, 0, 1,
	0, 1, 0, 1,
	0, 0, 0, 1
};




int main(int argc, char* args[])
{
	try
	{
		HandleArguments(argc, args);

		auto& image = Image::FromFile(FileIn);
		const int WindowWidth = image->GetWidth(), WindowHeight = image->GetHeight();


		glfwInit();
		GLFWwindow* window = glfwCreateWindow(WindowWidth, WindowHeight, "Canny", nullptr, nullptr);
		glfwMakeContextCurrent(window);
		InitOpenGL();

		glfwSetWindowSizeCallback(window, [](GLFWwindow* window, int x, int y) {
			glViewport(0, 0, x, y);
		});

		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		auto& texture = image->CreateTexture();
		FFQuad quad;
		auto& nullEffect = Effect::FromFile("Null", "pgm.glsl");
		auto& lblurEffect = Effect::FromFile("Linear Blur", "lblur.glsl");
		auto& sobelEffect = Effect::FromFile("Sobel", "sobel.glsl");
		auto& gaussEffect = Effect::FromFile("Gauss", "gauss.glsl");
		auto& channelEffect = Effect::FromFile("Channel", "channel.glsl");
		auto& nmsEffect = Effect::FromFile("NonMaximal Supression", "nms.glsl");



		FramebufferTexture leftBuffer(WindowWidth, WindowHeight, GL_RGBA32F, GL_RGBA);
		FramebufferTexture rightBuffer(WindowWidth, WindowHeight, GL_RGBA32F, GL_RGBA);

		fmat4 projection(glm::ortho<float>(0, WindowWidth, 0, WindowHeight));
		fmat4 trans(glm::scale<float>(WindowWidth, WindowHeight, 1));

		nullEffect->Bind();
		nullEffect->Uniform("InTexture", 0);
		nullEffect->Uniform("Projection", projection);
		nullEffect->Uniform("Transform", trans);

		lblurEffect->Bind();
		lblurEffect->Uniform("InTexture", 0);
		lblurEffect->Uniform("Projection", projection);
		lblurEffect->Uniform("Transform", trans);

		sobelEffect->Bind();
		sobelEffect->Uniform("InTexture", 0);
		sobelEffect->Uniform("CutOff", 0);
		sobelEffect->Uniform("Projection", projection);
		sobelEffect->Uniform("Transform", trans);
		sobelEffect->Uniform("LeftKernel", 
			fmat3(fvec3(1, 2, 1)/8.0f, fvec3(0, 0, 0)/8.0f, fvec3(-1, -2, -1)/8.0f));
		sobelEffect->Uniform("RightKernel", 
			fmat3(fvec3(1, 0, -1)/8.0f, fvec3(2, 0, -2)/8.0f, fvec3(1, 0, -1)/8.0f));

		gaussEffect->Bind();
		gaussEffect->Uniform("Sigma", 3);
		gaussEffect->Uniform("CutOff", 0);
		gaussEffect->Uniform("Projection", projection);
		gaussEffect->Uniform("Transform", trans);
		gaussEffect->Uniform("InTexture", 0);
		gaussEffect->Uniform("InTexTexels", 1.0f/fvec2(image->GetWidth(), image->GetHeight()));
		gaussEffect->Uniform("GaussTexture", 1);

		channelEffect->Bind();
		channelEffect->Uniform("InTexture", 0);
		channelEffect->Uniform("GrayScale", 1);
		channelEffect->Uniform("Projection", projection);
		channelEffect->Uniform("Transform", trans);
		channelEffect->Uniform("Channel", fvec4(0, 0, 0, 1));

		nmsEffect->Bind();
		nmsEffect->Uniform("Projection", projection);
		nmsEffect->Uniform("Transform", trans);
		nmsEffect->Uniform("InTexture", 0);
		nmsEffect->Uniform("PeakColor", fvec4(1));
		nmsEffect->Uniform("InTexTexels", 1.0f/fvec2(image->GetWidth(), image->GetHeight()));

		texture->Bind(GL_TEXTURE0);
		Effect* currentEffect, *endEffect;
		currentEffect = gaussEffect.get();
		endEffect = nmsEffect.get();
		
		float cutOff = 0, lowOff = 0;
		float divisor = 1;
		double framerate = 0;
		double time = 0;
		unique_ptr<char[]> windowTitle(new char[128]);
		
		while(glfwWindowShouldClose(window) == false)
		{
			bool cOffC = false;
			double timeDelta = glfwGetTime() - time;
			time = glfwGetTime();
			framerate = framerate * 0.8 + (1.0/timeDelta)*0.2;

			glfwPollEvents();
			if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			{
				glfwSetWindowShouldClose(window, true);
				continue;
			}

			float accel = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) ? 1 : .05;

			if(glfwGetKey(window, GLFW_KEY_F1))
				currentEffect = nullEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_F2))
				currentEffect = lblurEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_F3))
				currentEffect = sobelEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_F4))
				currentEffect = gaussEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_0))
				endEffect = nullEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_1))
				endEffect = channelEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_2))
				endEffect = nmsEffect.get();
			else if(glfwGetKey(window, GLFW_KEY_UP))
			{
				cutOff += accel * timeDelta;
				cOffC = true;
			}
			else if(glfwGetKey(window, GLFW_KEY_DOWN))
			{
				cutOff -= accel * timeDelta;
				cOffC = true;
			}
			else if(glfwGetKey(window, GLFW_KEY_RIGHT))
			{
				lowOff += accel*timeDelta;
				cOffC;
			}
			else if(glfwGetKey(window, GLFW_KEY_LEFT))
			{
				lowOff -= accel*timeDelta;
				cOffC;
			}

			{
				sprintf(windowTitle.get(), "Canny - CutOff : [%.4f, %.4f] - Frames %f - TPF : %f", lowOff, cutOff, framerate, timeDelta);
				glfwSetWindowTitle(window, windowTitle.get());
			}

			/*----------- Start Rendering Code --------------*/

			leftBuffer.BindWrite();
			texture->Bind(GL_TEXTURE0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			currentEffect->Bind();
			quad.Draw();
			

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			endEffect->Bind();
			endEffect->Uniform("HighThresh", cutOff);
			endEffect->Uniform("LowThresh", lowOff);
			leftBuffer.BindRead(GL_TEXTURE0);
			quad.Draw();

			/*------------ End Rendering Code ---------------*/
			glfwSwapBuffers(window);
			
		}

		glfwTerminate();
	}
	catch(std::exception& e)
	{
		std::cout << e.what() << std::endl;
		std::cin.get();
	}
}



