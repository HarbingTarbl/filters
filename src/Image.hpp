#pragma once
#include "CompiledHeader.hpp"
#include <SOIL.h>

class Texture;
class PGMTexture;

class Image
{
public:
	virtual unsigned GetWidth() const = 0;
	virtual unsigned GetHeight() const = 0;
	virtual unique_ptr<Texture> CreateTexture() = 0;
	static unique_ptr<Image> FromFile(const fi::path& path);

	~Image() {}
};

class SOILImage
	: public Image
{
	friend class SOILTexture;

private:
	SOILImage(const fi::path& path);

	int width;
	int height;
	int channels;

	unsigned long long bufferSize;
	unique_ptr<unsigned char> buffer;
public:
	~SOILImage() {}

	unsigned GetWidth() const { return width; }
	unsigned GetHeight() const { return height; }
	unsigned GetChannels() const { return channels; }

	unique_ptr<Texture> CreateTexture();

	static unique_ptr<SOILImage> FromFile(const fi::path& path);
	static unique_ptr<SOILImage> FromFramebuffer();
};

class PGMImage
	: public Image
{
	friend class PGMTexture;

private:
	PGMImage(const fi::path& path);

	vector<float> buffer;

	unsigned width;
	unsigned height;
public:

	unsigned short MaxValue;
	unsigned short ActualMax;
	unsigned PixelSize; //in bytes, either 1 or 2

	unsigned GetWidth() const { return width; }
	unsigned GetHeight() const { return height; }

	unique_ptr<Texture> CreateTexture();

	const float* Buffer() const
	{
		return &buffer[0];
	}

	~PGMImage()
	{

	}

	static unique_ptr<PGMImage> FromFile(const fi::path& path);
	static unique_ptr<PGMImage> FromFramebuffer();
};