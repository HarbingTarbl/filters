#include "CompiledHeader.hpp"
#include "Window.hpp"

bool Window::openglInitalized = false;
bool Window::glfwInitalized = false;

void Window::ProxyResize(GLFWwindow* window, int w, int h)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnResize(w, h);
}
void Window::ProxyMove(GLFWwindow* window, int x, int y)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnMove(x, y);
}
void Window::ProxyRefresh(GLFWwindow* window)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnRefresh();
}
void Window::ProxyFocus(GLFWwindow* window, int f)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnFocus(f);
}
void Window::ProxyIconified(GLFWwindow* window, int i)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnIconified(i);
}
void Window::ProxyClose(GLFWwindow* window)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnClose();
}
void Window::ProxyFBResize(GLFWwindow* window, int w, int h)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnFBResize(w, h);
}
void Window::ProxyMouseButton(GLFWwindow* window, int b, int a, int m)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnMouseButton(b, a, m);
}
void Window::ProxyScroll(GLFWwindow* window, double x, double y)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnScroll(x, y);
}
void Window::ProxyChar(GLFWwindow* window, unsigned p)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnChar(p);
}
void Window::ProxyEnter(GLFWwindow* window, int e)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnEnter(e);
}
void Window::ProxyMouseMove(GLFWwindow* window, double x, double y)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnMouseMove(x, y);
}
void Window::ProxyKey(GLFWwindow* window, int k, int s, int a, int m)
{
	static_cast<Window*>(glfwGetWindowUserPointer(window))->OnKey(k, s, a, m);
}

void Window::OnResize(int nwi, int nhei)
{
	width = nwi;
	height = nhei;
	glViewport(0, 0, width, height);
}
void Window::OnMove(int newx, int newy)
{ 
	xpos = newx;
	ypos = newy;
}
void Window::OnRefresh()
{ 

}
void Window::OnFocus(int focused)
{ 

}
void Window::OnIconified(int iconed)
{ 

}
void Window::OnClose()
{

}
void Window::OnFBResize(int nwidth, int nheight)
{ 

}
void Window::OnMouseButton(int button, int action, int mods)
{ 

}
void Window::OnScroll(double xscroll, double yscroll)
{ 

}
void Window::OnChar(unsigned int point)
{ 

}
void Window::OnEnter(int entered)
{ 

}
void Window::OnMouseMove(double newx, double newy)
{ 
	moux = newx;
	mouy = newy;
}
void Window::OnKey(int key, int scan, int action, int mods)
{ 

}

Window::Window(const string& title, int width, int height, GLFWwindow* share, GLFWmonitor* mon)
	: width(width), height(height), fbwidth(width), fbheight(height), moux(0), mouy(0)
{
	if(!glfwInitalized)
	{
		glfwInit();
		glfwInitalized = true;
	}

	handle = glfwCreateWindow(width, height, title.c_str(), mon, share);

	if(!openglInitalized)
	{		
		glfwMakeContextCurrent(handle);
		if(glewInit() != GLEW_OK)
			throw runtime_error("Unable to initalize opengl");
		else
			cout << "Loaded " << glGetString(GL_VERSION) << endl;
		openglInitalized = true;
	}

	glViewport(0, 0, width, height);
	glfwSetWindowUserPointer(handle, this);

	glfwSetWindowPosCallback(handle, ProxyMove);
	glfwSetWindowSizeCallback(handle, ProxyResize);
	glfwSetWindowCloseCallback(handle, ProxyClose);
	glfwSetWindowRefreshCallback(handle, ProxyRefresh);
	glfwSetWindowFocusCallback(handle, ProxyFocus);
	glfwSetWindowIconifyCallback(handle, ProxyIconified);
	glfwSetKeyCallback(handle, ProxyKey);
	glfwSetCharCallback(handle, ProxyChar);
	glfwSetMouseButtonCallback(handle, ProxyMouseButton);
	glfwSetCursorPosCallback(handle, ProxyMouseMove);
	glfwSetCursorEnterCallback(handle, ProxyEnter);
	glfwSetScrollCallback(handle, ProxyScroll);
}
Window::~Window()
{

}
GLFWwindow* Window::GetHandle() const
{
	return handle;
}
int Window::GetWidth() const
{
	return width;
}
int Window::GetHeight() const
{
	return height;
}
void Window::SetWidth(int width)
{
	glfwSetWindowSize(handle, width, height);
}
void Window::SetHeight(int height)
{
	glfwSetWindowSize(handle, width, height);
}
int Window::GetX() const
{
	return xpos;
}
int Window::GetY() const
{
	return ypos;
}
void Window::SetX(int x)
{
	glfwSetWindowPos(handle, x, ypos);
}
void Window::SetY(int y)
{
	glfwSetWindowPos(handle, xpos, y);
}
double Window::GetMouseX() const
{
	return moux;
}
double Window::GetMouseY() const
{
	return mouy;
}
void Window::SetMouseX(double x)
{
	glfwSetCursorPos(handle, x, mouy);
}
void Window::SetMouseY(double y)
{
	glfwSetCursorPos(handle, moux, y);
}
void Window::Show(bool visible)
{
	if(visible)
		glfwShowWindow(handle);
	else
		glfwHideWindow(handle);
}
bool Window::IsOpen() const
{
	return glfwWindowShouldClose(handle) == false;
}
void Window::Close()
{
	glfwSetWindowShouldClose(handle, 1);
}
void Window::SwapBuffer() const
{
	glfwSwapBuffers(handle);
}

