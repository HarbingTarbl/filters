uniform sampler2D InTexture;

uniform vec4 Channel;
uniform bool GrayScale;

uniform mat4 Projection;
uniform mat4 Transform;

smooth VSData vec2 TexPos;

#ifdef VERTEX_SHADER
in vec4 Vertex;

void main()
{
	gl_Position = Projection*Transform*Vertex;
	TexPos = Vertex.xy; //
	TexPos.y = 1 - Vertex.y;
}


#elif defined FRAGMENT_SHADER
out vec4 FsColor;

void main()
{
	if(GrayScale)
		FsColor = vec4(dot(texture(InTexture, TexPos),Channel));
	else
		FsColor = (texture(InTexture, TexPos)*Channel);
}

#endif