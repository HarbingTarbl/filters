#include "CompiledHeader.hpp"
#include "Effect.hpp"

Effect::Effect(const string& name, const string& file)
	: name(name)
{
#ifdef GLFX
	if(!glfxParseEffectFromFile(effectId, fi::path(file).string().c_str()))
		throw runtime_error("Could not parse " + file);

	programId = glfxCompileProgram(effectId, name.c_str());
	if(programId < 0)
		throw runtime_error("Could not create " + name);
#else
	auto vertex = glCreateShader(GL_VERTEX_SHADER);
	fstream sfile(file, fstream::in);
	sfile.seekg(0, fstream::end);
	string source;
	source.resize(sfile.tellg());
	sfile.seekg(0, fstream::beg);
	sfile.read(&source[0], source.size());
	sfile.close();

	vector<const char*> strings;

	strings.emplace_back("#version 130\n#define VERTEX_SHADER\n#define VSData out\n");
	strings.emplace_back(source.c_str());

	glShaderSource(vertex, 2, &strings[0], nullptr);
	glCompileShader(vertex);

	int compStat;
	glGetShaderiv(vertex, GL_COMPILE_STATUS, &compStat);
	if(compStat != GL_TRUE)
	{
		int errSize;
		glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &errSize);
		string errorMessage;
		errorMessage.resize(errSize);
		glGetShaderInfoLog(vertex, errSize, &errSize, &errorMessage[0]);
		throw runtime_error(file + " : " + errorMessage);

	}
	strings.clear();

	auto fragment = glCreateShader(GL_FRAGMENT_SHADER);

	strings.emplace_back("#version 130\n#define FRAGMENT_SHADER\n#define VSData in\n");
	strings.emplace_back(source.c_str());

	glShaderSource(fragment, 2, &strings[0], nullptr);
	glCompileShader(fragment);

	glGetShaderiv(fragment, GL_COMPILE_STATUS, &compStat);
	if(compStat != GL_TRUE)
	{
		int errSize;
		glGetShaderiv(fragment, GL_INFO_LOG_LENGTH, &errSize);
		string errorMessage;
		errorMessage.resize(errSize);
		glGetShaderInfoLog(fragment, errSize, &errSize, &errorMessage[0]);
		throw runtime_error(file + " : " + errorMessage);	
	}

	programId = glCreateProgram();
	glAttachShader(programId, vertex);
	glAttachShader(programId, fragment);

	glBindAttribLocation(programId, 0, "Vertex");
	glBindAttribLocation(programId, 1, "Normal");
	glBindAttribLocation(programId, 2, "Uv");

	glLinkProgram(programId);

	glGetProgramiv(programId, GL_COMPILE_STATUS, &compStat);
	if(compStat != GL_TRUE)
	{
		int errSize;
		glGetProgramiv(errSize, GL_INFO_LOG_LENGTH, &errSize);
		string errorMessage;
		errorMessage.resize(errSize);
		glGetProgramInfoLog(programId, errSize, &errSize, &errorMessage[0]);
		throw runtime_error(file + " : " + errorMessage);	
	}

	glDeleteShader(fragment);
	glDeleteShader(vertex);		
#endif

	int uniformCount, maxLength;

	glGetProgramiv(programId, GL_ACTIVE_UNIFORMS, &uniformCount);
	glGetProgramiv(programId, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLength);
	if(uniformCount == 0)
		return;

	unique_ptr<char[]> nameBuf(new char[maxLength]);

	for(unsigned i = 0; i < uniformCount; i++)
	{
		int nameLength;
		glGetActiveUniformName(programId, i, maxLength, &nameLength, nameBuf.get());
		uniforms.emplace(string(nameBuf.get(), nameLength), glGetUniformLocation(programId, nameBuf.get()));
	}
}

Effect::~Effect()
{
#ifdef GLFX
	glfxDeleteEffect(effectId);
#endif
	glDeleteProgram(programId);
}

void Effect::Bind()
{
	glUseProgram(programId);
}

unsigned Effect::Location(const string& name)
{
	auto& find = uniforms.find(name);
	if(find == uniforms.end())
		return -1;
	else
		return find->second;
}

unsigned Effect::ProgramId() const 
{
	return programId;
}

void  Effect::Uniform(const string& name, const fmat4& v) 
{
	glUniformMatrix4fv(Effect::Location(name), 1, false, glm::value_ptr(v));
}

void  Effect::Uniform(const string& name, const fmat3& v) 
{
	glUniformMatrix3fv(Effect::Location(name), 1, false, glm::value_ptr(v));
}


void  Effect::Uniform(const string& name, const fvec4& v) 
{
	glUniform4fv(Effect::Location(name), 1, glm::value_ptr(v));
}


void  Effect::Uniform(const string& name, const fvec3& v) 
{
	glUniform3fv(Effect::Location(name), 1, glm::value_ptr(v));
}

void  Effect::Uniform(const string& name, const fvec2& v) 
{
	glUniform2fv(Effect::Location(name), 1, glm::value_ptr(v));
}

void  Effect::Uniform(const string& name, const float v) 
{
	glUniform1f(Effect::Location(name), (v));
}

void Effect::Uniform(const string& name, const int v)
{
	glUniform1i(Effect::Location(name), v);
}

void Effect::Uniform(const string& name, const unsigned v)
{
	Uniform(name, (int)v);
}

unique_ptr<Effect> Effect::FromFile(const string& name, const string& file)
{
	return unique_ptr<Effect>(new Effect(name, file));
}