uniform sampler2D InTexture;
uniform mat4 Projection;
uniform mat4 Transform;
uniform vec2 TexelSize;
uniform uint BlockSize;

smooth VSData vec2 TexPos;

#ifdef VERTEX_SHADER
in vec4 Vertex;

void main()
{
	gl_Position = Projection*Transform*Vertex;
	TexPos = Vertex.xy; // [-1, 1] -> [0, 1]
}


#elif defined FRAGMENT_SHADER
out vec4 FsColor;

void main()
{
	FsColor = vec4(0);
	for(int x = 0; x < 3; x++)
		for(int y = 0; y < 3; y++)
			FsColor += textureOffset(InTexture, TexPos, ivec2(x, y));
			
	FsColor /= 9.0;
}

#endif